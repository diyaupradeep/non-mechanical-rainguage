# Non Mechanical Rainguage
Currently,weather elements rain is monitored using mechanical instruments like tipping buckets which require regular maintenence .
This project aims at producing locally maintainable  weather stations at cheaper cost ,low power capabilities while ensuring accurate and dependable 
results using Machine Learcning(ML) at the Edge.
![Non mechanical Raingauge-PoC](https://gitlab.com/diyaupradeep/non-mechanical-rainguage/-/raw/main/Images%20&%20Videos/Untitled_design_2_.png)
# List of Parameters recorded
# RAIN
- Loudness measurement
- Sound measurement
# Prerequisites
  - Arduino IDE [Tested]
  - Edge Impulse Software [Tested]
# Getting Started
  # RAIN
  - Connect the Components as shown in the [Wiring Diagram](https://gitlab.com/diyaupradeep/non-mechanical-rainguage/-/blob/main/Hardware/circuit_diagram.pdf)
  - Upload the [Data Collection Module](https://gitlab.com/diyaupradeep/non-mechanical-rainguage/-/blob/main/Software/Data%20collection%20module/ble_sd_csv_sensors_serial_monitor.ino) script to Arduino BLE Sense board.
  - Once the data collection Completed
  - Preprocess the data with required timestamp and labels.
  - Import the data into Edge Impulse software
  - Data Training
  - Impulse Designing
  - Model Training
  - Performance Evaluation
  - Deployment on board [Edge module](https://gitlab.com/diyaupradeep/non-mechanical-rainguage/-/blob/main/Software/Edge%20module/real_rain_analysis_inferencing.zip)
  


# Contributing
Instructions coming up soon.

# License
This project is licensed under the MIT License - see the LICENSE.md file for details
